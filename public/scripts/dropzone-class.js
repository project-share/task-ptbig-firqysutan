var uploadedDocumentMap = {}
      Dropzone.options.documentDropzone = {
        url: "{{ route('classes.storeMedia') }}",
         maxFilesize: 2, // MB
         addRemoveLinks: true,
         acceptedFiles: ".jpeg,.jpg,.png,.gif",
         headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
         },
         success: function(file, response) {
            $('form').append('<input type="hidden" name="photo[]" value="' + response.name + '">')
            uploadedDocumentMap[file.name] = response.name
         },
         removedfile: function(file) {
            file.previewElement.remove()
            var name = ''
            if (typeof file.file_name !== 'undefined') {
               name = file.file_name
            } else {
               name = uploadedDocumentMap[file.name]
            }
            $('form').find('input[name="photo[]"][value="' + name + '"]').remove()
         },
         init: function () {
            let files = JSON.parse(document
              .querySelector("#photoDump")
              .getAttribute("data-photo"));
            if (Object.keys(files).length) {
              for (var i in files) {
                var file = files[i];
                console.log(file);
                file = {
                  ...file,
                  width: 226,
                  height: 324,
                };
                this.options.addedfile.call(this, file);
                this.options.thumbnail.call(this, file, file.original_url);
                file.previewElement.classList.add("dz-complete");
        
                $("form").append(
                  '<input type="hidden" name="photo[]" value="' + file.file_name + '">'
                );
              }
            }
          }
      }