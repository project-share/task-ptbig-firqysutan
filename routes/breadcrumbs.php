<?php

// Dashboard
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('Dashboard', route('dashboard.index'));
});

Breadcrumbs::for('dashboard_home', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Home', '#');
});

Breadcrumbs::for('edit_profile', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Edit Profile', route('profile.edit'));
});

Breadcrumbs::for('change_password', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Change Password', route('password.edit'));
});

Breadcrumbs::for('edit_user', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Edit Profile', route('user.edit'));
});

Breadcrumbs::for('edit_password', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Change Password', route('password.edit'));
});

// Home > About
Breadcrumbs::for('file_manager', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Media Library', route('filemanager.index'));
});

//Dashboard > Roles 
Breadcrumbs::for('roles', function ($trail) {
    $trail->parent('dashboard');
    $trail->push("Roles", route('roles.index'));
});

Breadcrumbs::for('detail_role', function ($trail, $role) {
    $trail->parent('roles', $role);
    $trail->push($role->name, route('roles.show', ['role' => $role]));
});

Breadcrumbs::for('add_role', function ($trail) {
    $trail->parent('roles');
    $trail->push('Add Role', route('roles.create'));
});

Breadcrumbs::for('edit_role', function ($trail, $role) {
    $trail->parent('roles');
    $trail->push($role->name, route('roles.edit', ['role' => $role]));
});

//Dashboard > Users 
Breadcrumbs::for('users', function ($trail) {
    $trail->parent('dashboard');
    $trail->push("User", route('users.index'));
});

Breadcrumbs::for('add_user', function ($trail) {
    $trail->parent('users');
    $trail->push('Add User', route('users.create'));
});

Breadcrumbs::for('edit_new_user', function ($trail, $user) {
    $trail->parent('users');
    $trail->push($user->name, route('users.edit', ['user' => $user]));
});

//Dashboard > Post Category
Breadcrumbs::for('categories_posts', function ($trail) {
    $trail->parent('dashboard');
    $trail->push("Post Category", route('categories-post.index'));
});

Breadcrumbs::for('add_category', function ($trail) {
    $trail->parent('categories_posts');
    $trail->push('Add Post Category', route('categories-post.create'));
});

Breadcrumbs::for('detail_category', function ($trail, $categoriesPosts) {
    $trail->parent('categories_posts', $categoriesPosts);
    $trail->push($categoriesPosts->category_title, route('categories-post.show', ['categories_post' => $categoriesPosts]));
});

//Dashboard > Post Tag
Breadcrumbs::for('tags', function ($trail) {
    $trail->parent('dashboard');
    $trail->push("Post Tag", route('tags.index'));
});

Breadcrumbs::for('add_tag', function ($trail) {
    $trail->parent('tags');
    $trail->push('Add Post', route('tags.create'));
});

Breadcrumbs::for('detail_tag', function ($trail, $tag) {
    $trail->parent('tags', $tag);
    $trail->push($tag->tag_title, route('tags.show', ['tag' => $tag]));
});

//Dashboard > Posts
Breadcrumbs::for('posts', function ($trail) {
    $trail->parent('dashboard');
    $trail->push("Posts", route('posts.index'));
});

Breadcrumbs::for('add_post', function ($trail) {
    $trail->parent('posts');
    $trail->push('Add Post', route('posts.create'));
});

Breadcrumbs::for('detail_post', function ($trail, $post) {
    $trail->parent('posts', $post);
    $trail->push($post->post_title, route('posts.show', ['post' => $post]));
});