<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\ForgotPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes([
    'register' => false
]);

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth']], function () {
    Route::get('/', [App\Http\Controllers\PostController::class, 'index'])->name('dashboard.index');
    Route::group(['prefix' => 'filemanager'], function () {
        Route::get('/folder', [App\Http\Controllers\FileManagerController::class, 'index'])->name('filemanager.index');
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });
    Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [App\Http\Controllers\ProfileController::class, 'update'])->name('profile.update');

    Route::get('/password', [App\Http\Controllers\PasswordController::class, 'edit'])->name('password.edit');
    Route::patch('/password', [App\Http\Controllers\PasswordController::class, 'updatePassword'])->name('password.updatePassword');

    //Roles Route
    Route::get('/roles/select', [App\Http\Controllers\RoleController::class, 'select'])->name('roles.select');
    Route::resource('/roles', \App\Http\Controllers\RoleController::class);

    //User Route
    Route::resource('/users', \App\Http\Controllers\UserController::class);

    //Category Post Route
    Route::get('/categories-post/select', [App\Http\Controllers\CategoriesPostController::class, 'select'])->name('categoriespost.select');
    Route::resource('/categories-post', App\Http\Controllers\CategoriesPostController::class);

    //Tag Post Route
    Route::get('/tags/select', [App\Http\Controllers\TagController::class, 'select'])->name('tags.select');
    Route::resource('/tags', App\Http\Controllers\TagController::class);

    //Post Route
    Route::resource('/posts', App\Http\Controllers\PostController::class);

    //File Manager Route 
    Route::group(['prefix' => 'filemanager'], function () {
        Route::get('/folder', [App\Http\Controllers\FileManagerController::class, 'index'])->name('filemanager.index');
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });
   
});
