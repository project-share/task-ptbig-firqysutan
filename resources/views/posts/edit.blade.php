@extends('layouts.dashboard')

@section('navbar')
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl navbar-custom" id="navbarBlur" navbar-scroll="true">
  <div class="container-fluid py-1 px-3">
      <nav aria-label="breadcrumb">
          {{-- <h6 class="font-weight-bolder mb-0 text-white">Dashboard</h6> --}}
          {{ Breadcrumbs::render('add_post') }}
      </nav>
      <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4 logout-icon" id="navbar">
          <div class="pe-md-3 d-flex align-items-center">
              <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                  <i class='bx bx-log-out'></i>
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
              </form>
          </div>
          <div class="user-setting">
              <div class="imgBox">
                  <img src="{{ asset('images/placeholder/user.jpg') }}" alt="">
              </div>
          </div>
      </div>
  </div>
</nav>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
      <form action="{{ route('posts.update', ['post' => $post]) }}" method="POST">
          @method('PUT')
          @csrf
          <div class="card">
              <div class="card-body _card-body">
                  <div class="row d-flex align-items-stretch">
                      <div class="col-md-6">
                          <!-- title -->
                          <div class="form-group _form-group">
                              <label for="input_post_title" class="font-weight-bold">
                                  Title <span class="wajib">* </span>
                              </label>
                              <input id="input_post_title" value="{{ old('post_title', $post->post_title) }}" name="post_title" type="text" class="form-control @error('post_title') is-invalid @enderror" placeholder="" />
                              @error('post_title')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>

                          <!-- catgeory -->
                          <div class="form-group _form-group">
                              <label for="select_user_role" class="font-weight-bold">
                                  Category <span class="wajib">* </span>
                              </label>
                              <select id="select_user_role" name="category" data-placeholder="Choose category" class="custom-select @error('category') is-invalid @enderror">
                                  @if (old('category', $categorySelected))
                                  <option value="{{ old('category', $categorySelected)->id }}" selected>
                                      {{ old('category', $categorySelected)->category_title }}
                                  </option>
                                  @endif
                              </select>
                              @error('category')
                              <span class="invalid-feedback">
                                  {{ $message }}
                              </span>
                              @enderror
                              <!-- error message -->
                          </div>

                      </div>


                      <div class="col-md-6">
                          <!-- slug -->
                          <div class="form-group _form-group">
                              <label for="input_post_slug" class="font-weight-bold">
                                  Slug
                              </label>
                              <input id="input_post_slug" name="post_slug" type="text" class="form-control @error('post_slug') is-invalid @enderror" value="{{ old('post_slug', $post->post_slug) }}" readonly />
                              @error('post_slug')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>

                          <!-- thumbnail -->
                          <div class="form-group _form-group">
                              <label for="input_post_thumbnail" class="font-weight-bold">
                                  Image Cover <span class="wajib">* </span>
                              </label>
                              <div class="input-group">
                                  <div class="input-group-prepend">
                                      <button id="button_post_thumbnail" data-input="input_post_thumbnail" class="btn btn-primary" type="button" style="padding: 0px 10px 0px 10px">
                                          Browse
                                      </button>
                                  </div>
                                  <input id="input_post_thumbnail" name="post_thumbnail" value="{{ old('post_thumbnail', $post->post_thumbnail) }}" type="text" class="form-control @error('post_thumbnail') is-invalid @enderror" placeholder="" readonly />
                                  @error('post_thumbnail')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                                  @enderror
                              </div>
                          </div>

                      </div>
                  </div>

                  <div class="row">
                      <div class="col-12">
                          <!-- caption -->
                          <div class="form-group _form-group">
                              <label for="input_post_caption" class="font-weight-bold">
                                  Short Description <span class="wajib">* </span>
                              </label>
                              <textarea id="input_post_caption" name="post_excerpt" placeholder="" class="form-control @error('post_excerpt') is-invalid @enderror" rows="3">{{ old('post_excerpt', $post->post_excerpt) }}</textarea>
                              @error('post_excerpt')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>

                          <!-- description -->
                          <div class="form-group _form-group">
                              <label for="input_post_description" class="font-weight-bold">
                                  Description <span class="wajib">* </span>
                              </label>
                              <textarea id="input_post_description" name="post_desc" placeholder="" class="form-control @error('post_desc') is-invalid @enderror" rows="20">{{ old('post_desc', $post->post_desc) }}</textarea>
                              @error('post_desc')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>

                          <!-- tag -->
                          <div class="form-group _form-group">
                              <label for="select_post_tag" class="font-weight-bold">
                                  Tag
                              </label>
                              <select id="select_post_tag" name="tag[]" data-placeholder="" class="custom-select" multiple>
                                  @if (old('tag', $post->tags))
                                  @foreach (old('tag', $post->tags) as $tag)
                                  <option value="{{ $tag->id }}" selected>
                                      {{ $tag->tag_title }}
                                  </option>
                                  @endforeach
                                  @endif
                              </select>
                          </div>

                          <hr style="margin: 20px 0px">

                          <!-- meta_title -->
                          <div class="form-group _form-group">
                              <label for="input_post_meta_title" class="font-weight-bold">
                                  Meta Title (SEO)
                              </label>
                              <input id="input_post_meta_title" value="{{ old('post_meta_title', $post->post_meta_title) }}" name="post_meta_title" type="text" class="form-control @error('post_meta_title') is-invalid @enderror" placeholder="Write meta title here.." />
                              @error('post_meta_title')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>

                          <!-- meta_description -->
                          <div class="form-group _form-group">
                              <label for="input_post_meta_description" class="font-weight-bold">
                                  Meta Description (SEO)
                              </label>
                              <textarea id="input_post_meta_description" name="post_meta_description" placeholder="Max 150 Words | Write meta description here.." class="form-control @error('post_meta_description') is-invalid @enderror" rows="3">{{ old('post_meta_description', $post->post_meta_description) }}</textarea>
                              @error('post_meta_description')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>

                          <!-- meta_keyword -->
                          <div class="form-group _form-group">
                              <label for="input_post_meta_keyword" class="font-weight-bold">
                                  Meta Keyword (SEO)
                              </label>
                              <textarea id="input_post_meta_keyword" name="post_meta_keyword" value="{{ old('post_meta_keyword', $post->post_meta_keyword) }}" placeholder="Example: jasa, perusahaan, digital marketing, programming" class="form-control @error('post_meta_keyword') is-invalid @enderror" rows="3">{{ old('post_meta_keyword', $post->post_meta_keyword) }}</textarea>
                              @error('post_meta_keyword')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>

                          <!-- status -->
                          <div class="form-group _form-group{{ $errors->has('is_active') ? ' has-error' : '' }}" style="display:none;">
                              <label for="input_post_status" class="font-weight-bold">
                                  Status
                              </label>
                              <div class="form-group">
                                  <label class="switch">
                                      <input type="checkbox" name="is_active" {{ old('is_active', $post->is_active) == 1 ? 'checked' : null }}>
                                      <span class="slider round"></span>
                                  </label>
                              </div>
                          </div>

                          <!-- Status Alternative -->
                          <div class="form-group  _form-group">
                              <label for="select_user_role" class="font-weight-bold">
                                  Status
                              </label>

                              <select name="article_status" data-placeholder="Pilih Kategori" class="custom-select @error('article_status') is-invalid @enderror">
                                  <option {{old('article_status',$post->article_status)=="reviewed"? 'selected':''}} value="reviewed">Reviewed</option>
                                  <option {{old('article_status',$post->article_status)=="rejected"? 'selected':''}} value="rejected">Rejected</option>
                                  <option {{old('article_status',$post->article_status)=="published"? 'selected':''}} value="published">Published</option>
                                  <option {{old('article_status',$post->article_status)=="draft"? 'selected':''}} value="draft">Draft</option>
                              </select>
                              @error('article_status')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                              <!-- error message -->
                          </div>

                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="float-right">
                              <a class="btn btn-outline-secondary _btn-secondary px-4" href="{{ route('posts.index') }}">Back</a>
                              <button type="submit" class="btn btn-primary _btn-primary px-4">
                                  Update
                              </button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </form>
  </div>
</div>
@endsection

@push('css-external')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2-bootstrap4.min.css') }}">
@endpush

@push('javascript-external')
<script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
<script src="{{ asset('vendor/tinymce5/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('vendor/tinymce5/tinymce.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/' . app()->getLocale() . '.js') }}"></script>
@endpush

@push('javascript-internal')
<script>
  $(document).ready(function() {
      $("#input_post_title").change(function(event) {
          $("#input_post_slug").val(
              event.target.value
              .trim()
              .toLowerCase()
              .replace(/[^a-z\d-]/gi, "-")
              .replace(/-+/g, "-")
              .replace(/^-|-$/g, "")
          );
      });

      $('#button_post_thumbnail').filemanager('image');

      $("#input_post_description").tinymce({
          relative_urls: false,
          language: "en",
          plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table directionality",
              "emoticons template paste textpattern",
          ],
          toolbar1: "fullscreen preview",
          toolbar2: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",

          file_picker_callback: function(callback, value, meta) {
              let x = window.innerWidth || document.documentElement.clientWidth || document
                  .getElementsByTagName('body')[0].clientWidth;
              let y = window.innerHeight || document.documentElement.clientHeight || document
                  .getElementsByTagName('body')[0].clientHeight;

              let cmsURL =
                  "{{ route('unisharp.lfm.show') }}" +
                  '?editor=' + meta.fieldname;
              if (meta.filetype == 'image') {
                  cmsURL = cmsURL + "&type=Images";
              } else {
                  cmsURL = cmsURL + "&type=Files";
              }

              tinyMCE.activeEditor.windowManager.openUrl({
                  url: cmsURL,
                  title: 'Filemanager',
                  width: x * 0.8,
                  height: y * 0.8,
                  resizable: "yes",
                  close_previous: "no",
                  onMessage: (api, message) => {
                      callback(message.content);
                  }
              });
          }
      });
      //select2 tag
      $('#select_post_tag').select2({
          theme: 'bootstrap4',
          language: "",
          allowClear: true,
          ajax: {
              url: "{{ route('tags.select') }}",
              dataType: 'json',
              delay: 250,
              processResults: function(data) {
                  return {
                      results: $.map(data, function(item) {
                          return {
                              text: item.tag_title,
                              id: item.id
                          }
                      })
                  };
              }
          }
      });
      //select2 tag
      $('#select_user_role').select2({
          theme: 'bootstrap4',
          language: "",
          allowClear: true,
          ajax: {
              url: "{{ route('categoriespost.select') }}",
              dataType: 'json',
              delay: 250,
              processResults: function(data) {
                  return {
                      results: $.map(data, function(item) {
                          return {
                              text: item.category_title,
                              id: item.id
                          }
                      })
                  };
              }
          }
      });
  });
</script>
@endpush
