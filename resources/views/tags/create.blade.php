@extends('layouts.dashboard')

@section('navbar')
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl navbar-custom" id="navbarBlur" navbar-scroll="true">
  <div class="container-fluid py-1 px-3">
      <nav aria-label="breadcrumb">
          {{-- <h6 class="font-weight-bolder mb-0 text-white">Dashboard</h6> --}}
          {{ Breadcrumbs::render('add_tag') }}
      </nav>
      <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4 logout-icon" id="navbar">
          <div class="pe-md-3 d-flex align-items-center">
              <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                  <i class='bx bx-log-out'></i>
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
              </form>
          </div>
          <div class="user-setting">
              <div class="imgBox">
                  <img src="{{ asset('images/placeholder/user.jpg') }}" alt="">
              </div>
          </div>
      </div>
  </div>
</nav>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
      <div class="card">
          <div class="card-body _card-body">
              <form action="{{ route('tags.store') }}" method="POST">
                  @csrf
                  <div class="row">
                      <div class="col-md-3"></div>
                      <div class="col-md-6">
                          <!-- title -->
                          <div class="form-group _form-group">
                              <label for="input_tag_title" class="font-weight-bold">
                                  Name <span class="wajib">*</span>
                              </label>
                              <input id="input_tag_title" value="{{ old('tag_title') }}" name="tag_title" type="text" class="form-control @error('tag_title') is-invalid @enderror" placeholder="Write name here.." />
                              @error('tag_title')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- slug -->
                          <div class="form-group _form-group">
                              <label for="input_tag_slug" class="font-weight-bold">
                                  Slug
                              </label>
                              <input id="input_tag_slug" value="{{ old('tag_slug') }}" name="tag_slug" type="text" class="form-control @error('tag_slug') is-invalid @enderror" placeholder="Auto Generate" readonly />
                              @error('tag_slug')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>

                          <!-- status -->
                          <div class="form-group{{ $errors->has('is_active') ? ' has-error' : '' }} _form-group">
                              <label for="input_post_status" class="font-weight-bold">
                                  Status
                              </label>
                              <div class="form-group">
                                  <label class="switch">
                                      <input type="checkbox" name="is_active" {{ old('is_active') == 1 ? 'checked' : null }}>
                                      <span class="slider-switch round"></span>
                                  </label>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-3"></div>

                  </div>

                  <div class="row">
                      <div class="col-md-3"></div>
                      <div class="col-md-6">
                          <button type="submit" class="btn btn-primary _btn-primary float-right px-4">
                              Save
                          </button>
                          <div class="float-right">
                              <a class="btn btn-outline-secondary _btn-secondary px-4 mx-2" href="{{ route('tags.index') }}">
                                  Back
                              </a>
                          </div>
                      </div>
                      <div class="col-md-3"></div>
                  </div>

              </form>
          </div>
      </div>
  </div>
</div>
@endsection

@push('javascript-external')
<script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
@endpush

@push('javascript-internal')
<script>
  $(document).ready(function() {
      $("#input_tag_title").change(function(event) {
          $("#input_tag_slug").val(
              event.target.value
              .trim()
              .toLowerCase()
              .replace(/[^a-z\d-]/gi, "-")
              .replace(/-+/g, "-")
              .replace(/^-|-$/g, "")
          );
      });

      $('#button_banner_image').filemanager('image');

      $("#input_banner_description").tinymce({
          relative_urls: false,
          language: "en",
          plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table directionality",
              "emoticons template paste textpattern",
          ],
          toolbar1: "fullscreen preview",
          toolbar2: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",

          file_picker_callback: function(callback, value, meta) {
              let x = window.innerWidth || document.documentElement.clientWidth || document
                  .getElementsByTagName('body')[0].clientWidth;
              let y = window.innerHeight || document.documentElement.clientHeight || document
                  .getElementsByTagName('body')[0].clientHeight;

              let cmsURL =
                  "{{ route('unisharp.lfm.show') }}" +
                  '?editor=' + meta.fieldname;
              if (meta.filetype == 'image') {
                  cmsURL = cmsURL + "&type=Images";
              } else {
                  cmsURL = cmsURL + "&type=Files";
              }

              tinyMCE.activeEditor.windowManager.openUrl({
                  url: cmsURL,
                  title: 'Filemanager',
                  width: x * 0.8,
                  height: y * 0.8,
                  resizable: "yes",
                  close_previous: "no",
                  onMessage: (api, message) => {
                      callback(message.content);
                  }
              });
          }
      });
      //select2 tag

  });
</script>
@endpush
