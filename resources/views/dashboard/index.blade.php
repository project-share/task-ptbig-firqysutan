@extends('layouts.dashboard')

@section('navbar')
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl navbar-custom" id="navbarBlur"
        navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                {{-- <h6 class="font-weight-bolder mb-0 text-white">Dashboard</h6> --}}
                {{ Breadcrumbs::render('dashboard') }}
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4 logout-icon" id="navbar">
                <div class="pe-md-3 d-flex align-items-center">
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                        <i class='bx bx-log-out'></i>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
                <div class="user-setting">
                    <div class="imgBox">
                        <img src="{{ asset('images/placeholder/user.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </nav>
@endsection

@section('content')
    <div class="row mb-4">
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-dark shadow-dark text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Published</p>
                        <h4 class="mb-0">10 Post</h4>
                    </div>
                </div>
                <hr class="dark horizontal my-0">
                <div class="card-footer p-3">
                    <p class="mb-0 link-more"><a href="#">SELENGKAPNYA <i class='bx bx-right-arrow-circle'></i></a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">person</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Draft</p>
                        <h4 class="mb-0">17 Post</h4>
                    </div>
                </div>
                <hr class="dark horizontal my-0">
                <div class="card-footer p-3">
                    <p class="mb-0 link-more"><a href="#">SELENGKAPNYA <i class='bx bx-right-arrow-circle'></i></a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">person</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Total Visitors</p>
                        <h4 class="mb-0">3,462 Visit</h4>
                    </div>
                </div>
                <hr class="dark horizontal my-0">
                <div class="card-footer p-3">
                    <p class="mb-0 link-more"><a href="#">SELENGKAPNYA <i class='bx bx-right-arrow-circle'></i></a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Member</p>
                        <h4 class="mb-0">230 Orang</h4>
                    </div>
                </div>
                <hr class="dark horizontal my-0">
                <div class="card-footer p-3">
                    <p class="mb-0 link-more"><a href="#">SELENGKAPNYA <i class='bx bx-right-arrow-circle'></i></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="row mb-4">
        <div class="col-lg-8 col-md-6 mb-md-0 mb-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">

                            <h4>Newest Article</h4>

                        </div>

                        <div class="card-body">
                            <ul class="list-group list-group-flush">
                                <!-- card-header -->
                                <div class="table-responsive table-striped">
                                    <table class="table mg-b-0 tx-13">
                                        <thead>
                                            <tr class="tx-10">
                                                <th style="width: 20%;" class="pd-y-5">Title</th>
                                                <th style="width: 40%; max-width: 40%" class="pd-y-5">Description</th>
                                                <th style="width: 10%;" class="pd-y-5 tx-center t-center">Category</th>
                                                <th style="width: 10%;" class="pd-y-5 tx-center t-center">Date</th>
                                                <th style="width: 10%;" class="pd-y-5 tx-center t-center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($posts as $post)
                                                <tr>
                                                    <td style="width: 20%;"
                                                        class="pd-y-5 valign-middle tx-medium tx-inverse tx-14">
                                                        {{ $post->post_title }}

                                                    <td style="width: 40%;"
                                                        class="pd-y-5 valign-middle tx-medium tx-inverse tx-14">
                                                        {!! $post->post_excerpt !!}
                                                    </td>

                                                    <td style="width: 15%;"
                                                        class="pd-y-5 t-center tx-center valign-middle tx-medium tx-inverse tx-14">
                                                        {{ $post->category->category_title }}
                                                    </td>

                                                    <td style="width: 15%;"
                                                        class="pd-y-5 t-center tx-center valign-middle tx-medium tx-inverse tx-14">
                                                        {{ date('d, M Y', strtotime($post->created_at)) }}
                                                    </td>

                                                    <td style="width: 10%;"
                                                        class="pd-y-5 t-center valign-middle tx-center">
                                                        @if ($post->is_active == 1)
                                                            <span class="status-active">Active</span>
                                                        @else
                                                            <span class="status-nonactive">Non-Active</span>
                                                        @endif
                                                    </td>

                                                </tr>
                                            @empty
                                                <table>

                                                </table>
                                                <p style="text-align: center; padding-top: 50px;">
                                                    @if (request()->get('keyword'))
                                                        <strong>Article not found</strong>
                                                    @else
                                                        <strong>No article data yet</strong>
                                                    @endif

                                                </p>
                                            @endforelse

                                        </tbody>
                                    </table>
                                </div>
                                <!-- table-responsive -->

                            </ul>
                        </div>
                        <!-- table-responsive -->
                        @if ($posts->hasPages())
                            <div class="card-footer">
                                {{ $posts->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card h-100">
                <div class="card-header pb-0">
                    <h6>Orders overview</h6>
                    <p class="text-sm">
                        <i class="fa fa-arrow-up text-success" aria-hidden="true"></i>
                        <span class="font-weight-bold">24%</span> this month
                    </p>
                </div>
                <div class="card-body p-3">
                    <div class="timeline timeline-one-side">
                        <div class="timeline-block mb-3">
                            <span class="timeline-step">
                                <i class="material-icons text-success text-gradient">notifications</i>
                            </span>
                            <div class="timeline-content">
                                <h6 class="text-dark text-sm font-weight-bold mb-0">$2400, Design changes</h6>
                                <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">22 DEC 7:20 PM</p>
                            </div>
                        </div>
                        <div class="timeline-block mb-3">
                            <span class="timeline-step">
                                <i class="material-icons text-danger text-gradient">code</i>
                            </span>
                            <div class="timeline-content">
                                <h6 class="text-dark text-sm font-weight-bold mb-0">New order #1832412</h6>
                                <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">21 DEC 11 PM</p>
                            </div>
                        </div>
                        <div class="timeline-block mb-3">
                            <span class="timeline-step">
                                <i class="material-icons text-info text-gradient">shopping_cart</i>
                            </span>
                            <div class="timeline-content">
                                <h6 class="text-dark text-sm font-weight-bold mb-0">Server payments for April</h6>
                                <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">21 DEC 9:34 PM</p>
                            </div>
                        </div>
                        <div class="timeline-block mb-3">
                            <span class="timeline-step">
                                <i class="material-icons text-warning text-gradient">credit_card</i>
                            </span>
                            <div class="timeline-content">
                                <h6 class="text-dark text-sm font-weight-bold mb-0">New card added for order #4395133</h6>
                                <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">20 DEC 2:20 AM</p>
                            </div>
                        </div>
                        <div class="timeline-block mb-3">
                            <span class="timeline-step">
                                <i class="material-icons text-primary text-gradient">key</i>
                            </span>
                            <div class="timeline-content">
                                <h6 class="text-dark text-sm font-weight-bold mb-0">Unlock packages for development</h6>
                                <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">18 DEC 4:54 AM</p>
                            </div>
                        </div>
                        <div class="timeline-block">
                            <span class="timeline-step">
                                <i class="material-icons text-dark text-gradient">payments</i>
                            </span>
                            <div class="timeline-content">
                                <h6 class="text-dark text-sm font-weight-bold mb-0">New order #9583120</h6>
                                <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">17 DEC</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
