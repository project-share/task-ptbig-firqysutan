<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('Manage Posts', function ($user) {
            return $user->hasAnyPermission([
                'Post Show',
                'Post Create',
                'Post Update',
                'Post Delete'
            ]);
        });

        Gate::define('Manage Post Categories', function ($user) {
            return $user->hasAnyPermission([
                'Post Category Show',
                'Post Category Create',
                'Post Category Update',
                'Post Category Delete'
            ]);
        });

        Gate::define('Manage Tags', function ($user) {
            return $user->hasAnyPermission([
                'Tag Show',
                'Tag Create',
                'Tag Update',
                'Tag Delete'
            ]);
        });

        Gate::define('Manage Profiles', function ($user) {
            return $user->hasAnyPermission([
                'Profile Show',
                'Profile Create',
                'Profile Update',
                'Profile Delete'
            ]);
        });

        Gate::define('Manage Passwords', function ($user) {
            return $user->hasAnyPermission([
                'Password Show',
                'Password Create',
                'Password Update',
                'Password Delete'
            ]);
        });

        Gate::define('Manage Roles', function ($user) {
            return $user->hasAnyPermission([
                'Role Show',
                'Role Create',
                'Role Update',
                'Role Delete'
            ]);
        });

        Gate::define('Manage Users', function ($user) {
            return $user->hasAnyPermission([
                'User Show',
                'User Create',
                'User Update',
                'User Delete'
            ]);
        });
    }
}
