<?php

namespace App\Models;

use DigitalCloud\Blameable\Traits\Blameable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriesPost extends Model
{
    use HasFactory;
    use Blameable;
    protected $table = 'post_categories';
    protected $fillable = ['category_title', 'category_slug', 'category_desc', 'category_thumbnail', 'is_active'];

    public function scopeOnlyParent($query)
    {
        return $query->whereNull('id');
    }

    public function scopeSearch($query, $title)
    {
        return $query->where('category_title', 'LIKE', "%{$title}%");
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function getRouteKeyName()
    {
        return 'category_slug';
    }
}
