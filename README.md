<h1>Step Configuration</h1>
<br><br>

## Database Import

You can Import Database in `Sql Folder db-task.sql`

## Setting .env

Set `.env` database (`DB_CONNECTION`, `DB_HOST`, `DB_PORT`, `DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD`)

## Open PHP Folder and Open php.ini file

`php.ini -> active extension: exif & gd2`

## Open Terminal or Command Prompt, type:

Then:
`composer install`

Then:
`composer update`

Then:
`php artisan key:generate`

Then:
`php artisan storage:link`

Finally:
`php artisan serve`
